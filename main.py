COLORS = [i + 31 for i in range(5)] * 4


def input_fun():
    temporary_variable = 6
    rings = int(input('Введите количество дисков от 3 до 20: '))
    towers = int(input('Введите количество башен от 3 до 10: '))
    start_t = int(input('Введите номер начальной башни: '))
    finish_t = int(input('Введите номер конечной башни: '))
    while True:
        temp = temporary_variable - start_t - finish_t
        if temp <= 0 or temp == start_t or temp == finish_t:
            temporary_variable += 1
        else:
            break
    towers = generate_start_towers(rings, towers, start_t)
    hanoi_fun(rings, start_t, finish_t, towers, temporary_variable, rings)


def generate_start_towers(n_rings, n_towers, start):
    towers = [[] for _ in range(n_towers)]
    towers[start - 1] = [i + 1 for i in reversed(range(n_rings))]
    print_towers(towers, n_rings)
    return towers


def hanoi_fun(n, start, finish, towers, temporary_variable, count_rings):
    if n <= 0:
        return
    temp = temporary_variable - start - finish
    hanoi_fun(n - 1, start, temp, towers, temporary_variable, count_rings)
    towers[finish - 1].append(towers[start - 1][-1])
    towers[start - 1].pop(-1)
    print_towers(towers, count_rings)
    hanoi_fun(n - 1, temp, finish, towers, temporary_variable, count_rings)


def print_towers(towers, count_rings):
    input()
    for i in reversed(range(count_rings)):
        for j in range(len(towers)):
            try:
                string = int(towers[j][i]) * '✦'
                color_code = f'\033[{COLORS[towers[j][i] - 1]}m ' + '{:<' + str(count_rings) + '}'
                print(color_code.format(string), end='')
            except IndexError:
                format_string = '\033[37m {:<' + str(count_rings) + '}'
                print(format_string.format('⋮'), end='')
        print()
    print()


if __name__ == '__main__':
    input_fun()
